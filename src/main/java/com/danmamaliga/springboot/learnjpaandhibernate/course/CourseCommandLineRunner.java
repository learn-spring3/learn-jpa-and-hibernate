package com.danmamaliga.springboot.learnjpaandhibernate.course;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.danmamaliga.springboot.learnjpaandhibernate.course.jdbc.CourseJdbcRepository;
import com.danmamaliga.springboot.learnjpaandhibernate.course.jpa.CourseJpaRepository;
import com.danmamaliga.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;

/**
 * Command Line Runner to test database operations over H2 database by different
 * methods: Spring JDBC, JPA, Spring Data JPA
 */
@Component
public class CourseCommandLineRunner implements CommandLineRunner {

	@Autowired
	private CourseJdbcRepository courseJdbcRepository;
	@Autowired
	private CourseJpaRepository courseJpaRepository;
	@Autowired
	private CourseSpringDataJpaRepository courseSpringDataJpaRepository;

	@Override
	public void run(String... args) throws Exception {
//     runWithJdbcRepository(courseJdbcRepository);
//     runWithJpaRepository(courseJpaRepository);
		runWithSpringDataJpaRepository(courseSpringDataJpaRepository);
	}

	private void runWithJdbcRepository(CourseJdbcRepository repository) {
		repository.insert(new Course(1, "Learn Spring", "danmamaliga"));
		repository.insert(new Course(2, "Learn AWS", "danmamaliga"));
		repository.insert(new Course(3, "Learn Microservices", "danmamaliga"));

		repository.deleteById(1);

		Course course = repository.findById(2);
		System.out.println(course);
	}

	private void runWithJpaRepository(CourseJpaRepository repository) {
		repository.insert(new Course(1, "Learn Spring", "danmamaliga"));
		repository.insert(new Course(2, "Learn AWS", "danmamaliga"));
		repository.insert(new Course(3, "Learn Microservices", "danmamaliga"));

		repository.deleteById(1);

		Course course = repository.findById(2);
		System.out.println(course);
	}

	private void runWithSpringDataJpaRepository(CourseSpringDataJpaRepository repository) {
		String author = "danmamaliga";
		repository.save(new Course(1, "Learn Spring", author));
		repository.save(new Course(2, "Learn AWS", author));
		repository.save(new Course(3, "Learn Microservices", author));

		repository.deleteById(1l);

		Course course = repository.findById(2l).get();
		System.out.println(course);

		System.out.println(repository.findAll());
		System.out.println("Number of courses: %s".formatted(repository.count()));

		List<Course> coursesByAuthor = repository.findByAuthor(author);
		System.out.println("All courses by author %s: %s".formatted(author, coursesByAuthor));

		String courseName = "Learn AWS";
		List<Course> coursesByName = repository.findByName(courseName);
		System.out.println("All courses by name %s: %s".formatted(courseName, coursesByName));
	}

}
